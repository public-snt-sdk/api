/// <reference types="qs" />
/// <reference types="express" />
import { StrategyOptions } from 'passport-jwt';
import { Access, GetRolesScopesResult, JwtPayload, RolesWithScopes, UserServiceForJwtAuth } from '../types/auth.interface';
export declare const jwtStrategy: (name: string) => {
    new (options: StrategyOptions, usersService: UserServiceForJwtAuth, rolesWithScopes?: RolesWithScopes, roleNameKeyInAccess?: string, firstScopesKey?: string, secondScopesKey?: string, getRolesScopes?: ((accesses: Access[]) => GetRolesScopesResult) | undefined): {
        usersService: UserServiceForJwtAuth;
        roleNameKeyInAccess: string;
        firstScopesKey: string;
        secondScopesKey: string;
        rolesWithScopes: RolesWithScopes;
        getRolesScopes: (accesses: Access[]) => GetRolesScopesResult;
        validate(payload: JwtPayload): Promise<import("../types/auth.interface").AuthUser>;
        name: string;
        authenticate(req: import("express").Request<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>, options?: any): void;
        success(user: any, info?: any): void;
        fail(challenge: any, status: number): void;
        fail(status: number): void;
        redirect(url: string, status?: number | undefined): void;
        pass(): void;
        error(err: Error): void;
    };
};
