import { Profile, Strategy, StrategyOptionsWithRequest, VerifyCallback } from 'passport-google-oauth20';
import { AuthUser, UserServiceForGoogleAuth } from '../types/auth.interface';
import { Request } from 'express';
declare const GoogleStrategy_base: new (...args: any[]) => Strategy;
export declare class GoogleStrategy extends GoogleStrategy_base {
    constructor(options: StrategyOptionsWithRequest, usersService: UserServiceForGoogleAuth);
    usersService: UserServiceForGoogleAuth;
    validate: (req: Request, accessToken: string, refreshToken: string, profile: Profile, done: VerifyCallback) => Promise<AuthUser>;
}
export {};
