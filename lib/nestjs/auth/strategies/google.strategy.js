"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoogleStrategy = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const passport_google_oauth20_1 = require("passport-google-oauth20");
class GoogleStrategy extends passport_1.PassportStrategy(passport_google_oauth20_1.Strategy, 'google') {
    constructor(options, usersService) {
        super(options);
        this.validate = (req, accessToken, refreshToken, profile, done) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            try {
                const state = (_a = req === null || req === void 0 ? void 0 : req.query) === null || _a === void 0 ? void 0 : _a.state;
                const { id: googleID, name, emails, photos, } = profile;
                const email = emails && emails.length && emails[0] && emails[0].value;
                const profilePhoto = photos && photos.length && photos[0] && photos[0].value;
                const query = {
                    email: email,
                    googleID,
                    firstName: name === null || name === void 0 ? void 0 : name.familyName,
                    lastName: name === null || name === void 0 ? void 0 : name.givenName,
                    profilePhoto: profilePhoto,
                    state: state,
                };
                const user = yield this.usersService.handleGoogleUser(query);
                if (!user) {
                    throw new common_1.UnauthorizedException();
                }
                const responseData = {
                    userID: user.userID,
                    firstName: name === null || name === void 0 ? void 0 : name.familyName,
                    lastName: name === null || name === void 0 ? void 0 : name.givenName,
                    profilePhoto,
                    googleID,
                    email,
                    status: user.status,
                    state,
                };
                return responseData;
            }
            catch (error) {
                return Promise.reject(error);
            }
        });
        this.usersService = usersService;
    }
}
exports.GoogleStrategy = GoogleStrategy;
//# sourceMappingURL=google.strategy.js.map