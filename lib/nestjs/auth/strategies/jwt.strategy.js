"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jwtStrategy = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const passport_jwt_1 = require("passport-jwt");
const helpers_1 = require("../helpers");
exports.jwtStrategy = (name) => {
    return class JwtStrategy extends passport_1.PassportStrategy(passport_jwt_1.Strategy, name) {
        constructor(options, usersService, rolesWithScopes = {}, roleNameKeyInAccess = 'role', firstScopesKey = 'company', secondScopesKey = 'department', getRolesScopes) {
            super(Object.assign({}, options));
            this.getRolesScopes = (accesses) => {
                const roles = [];
                const personalScopes = [];
                let firstScopes = {};
                let secondScopes = {};
                if (!accesses || !accesses.length) {
                    return {
                        roles,
                        personalScopes,
                        firstScopes,
                        secondScopes,
                    };
                }
                for (const access of accesses) {
                    if (!access) {
                        continue;
                    }
                    roles.push(access[this.roleNameKeyInAccess]);
                    switch (true) {
                        case !!access[`${this.firstScopesKey}ID`] && !!access[`${this.secondScopesKey}ID`]:
                            secondScopes = helpers_1.checkAndAddScopesToAccess({
                                accessAndScopes: secondScopes,
                                accessKey: access[`${this.secondScopesKey}ID`],
                                accessScopes: this.rolesWithScopes[access[this.roleNameKeyInAccess]],
                            });
                            continue;
                        case !!access[`${this.firstScopesKey}ID`]:
                            firstScopes = helpers_1.checkAndAddScopesToAccess({
                                accessAndScopes: firstScopes,
                                accessKey: access[`${this.firstScopesKey}ID`],
                                accessScopes: this.rolesWithScopes[access[this.roleNameKeyInAccess]],
                            });
                            continue;
                        default:
                            personalScopes.push(...this.rolesWithScopes[access[this.roleNameKeyInAccess]]);
                            continue;
                    }
                }
                return {
                    roles,
                    personalScopes,
                    firstScopes,
                    secondScopes,
                };
            };
            this.usersService = usersService;
            this.roleNameKeyInAccess = roleNameKeyInAccess;
            this.firstScopesKey = firstScopesKey;
            this.secondScopesKey = secondScopesKey;
            this.rolesWithScopes = rolesWithScopes;
            if (getRolesScopes)
                this.getRolesScopes = getRolesScopes;
        }
        validate(payload) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const userData = yield this.usersService.getCurrentUserCredentials({
                        userID: payload.userID,
                    });
                    if (!userData) {
                        throw new common_1.UnauthorizedException();
                    }
                    if (userData.status === 'INACTIVE') {
                        throw new common_1.UnauthorizedException('You need to activate your account by confirming your email');
                    }
                    if (userData.changePasswordAt &&
                        userData.changePasswordAt !== payload.changePasswordAt) {
                        throw new common_1.UnauthorizedException('Your token is expired');
                    }
                    const scopesData = this.getRolesScopes(userData.access);
                    const { roles, personalScopes, firstScopes, secondScopes, } = scopesData;
                    userData.roles = roles;
                    userData.personalScopes = personalScopes;
                    userData[`${this.firstScopesKey}Scopes`] = firstScopes;
                    userData[`${this.secondScopesKey}Scopes`] = secondScopes;
                    return userData;
                }
                catch (error) {
                    return Promise.reject(error);
                }
            });
        }
    };
};
//# sourceMappingURL=jwt.strategy.js.map