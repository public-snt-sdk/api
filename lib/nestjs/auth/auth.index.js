"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var google_strategy_1 = require("./strategies/google.strategy");
Object.defineProperty(exports, "GoogleStrategy", { enumerable: true, get: function () { return google_strategy_1.GoogleStrategy; } });
var jwt_strategy_1 = require("./strategies/jwt.strategy");
Object.defineProperty(exports, "jwtStrategy", { enumerable: true, get: function () { return jwt_strategy_1.jwtStrategy; } });
//# sourceMappingURL=auth.index.js.map