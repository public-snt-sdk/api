export interface HandleGoogleUserQuery {
    email: string;
    googleID: string;
    userID?: string;
    firstName?: string;
    lastName?: string;
    profilePhoto?: string;
    _id?: string;
    state?: string | string[] | undefined;
}
export interface Access {
    [key: string]: string | string[];
}
export interface AuthUser {
    userID?: string;
    access?: Access[];
    [key: string]: string | string[] | number | undefined | object;
}
export interface UserServiceForGoogleAuth {
    handleGoogleUser: (query: HandleGoogleUserQuery) => Promise<AuthUser>;
}
export interface UserServiceForJwtAuth {
    getCurrentUserCredentials: (query: GetCurrentUserCredentialsQuery) => Promise<AuthUser>;
}
export interface RolesWithScopes {
    [key: string]: string[] | string;
}
export interface Scopes {
    [key: string]: string[] | string;
}
export interface GetRolesScopesResult {
    roles: string[];
    personalScopes: string[];
    firstScopes: Scopes;
    secondScopes: Scopes;
}
export interface JwtPayload {
    userID: string;
    changePasswordAt?: string;
    changePinAt?: string;
}
export interface GetCurrentUserCredentialsQuery {
    email?: string;
    googleID?: string;
    userID?: string;
    firstName?: string;
    lastName?: string;
    profilePhoto?: string;
    _id?: string;
}
export interface AccessAndScopes {
    [key: string]: string[];
}
export interface CheckAndAddScopesToAccess {
    accessAndScopes: AccessAndScopes;
    accessKey: string;
    accessScopes: string[];
}
