import { CheckAndAddScopesToAccess } from './types/auth.interface';
export declare const checkAndAddScopesToAccess: ({ accessAndScopes, accessKey, accessScopes, }: CheckAndAddScopesToAccess) => import("./types/auth.interface").AccessAndScopes;
