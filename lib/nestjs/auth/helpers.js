"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAndAddScopesToAccess = void 0;
exports.checkAndAddScopesToAccess = ({ accessAndScopes = {}, accessKey, accessScopes, }) => {
    if (!accessKey || !accessScopes) {
        return accessAndScopes;
    }
    if (!accessAndScopes[accessKey]) {
        accessAndScopes[accessKey] = accessScopes;
        return accessAndScopes;
    }
    accessAndScopes[accessKey] = accessAndScopes[accessKey].concat(...accessScopes);
    return accessAndScopes;
};
//# sourceMappingURL=helpers.js.map