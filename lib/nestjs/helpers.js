"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.capitalizeFirstLetter = exports.getErrorsInProd = exports.getErrorsInLocal = exports.checkScope = void 0;
const hasScopeInScopeData = ({ scopeData, scopes }) => {
    if (!scopeData || !scopes) {
        return false;
    }
    for (const scope of scopes) {
        if (!scopeData.includes(scope)) {
            continue;
        }
        return true;
    }
    return false;
};
exports.checkScope = ({ scopes, requiredScopes }) => {
    if (!scopes || scopes.length === 0) {
        return false;
    }
    const hasScopes = requiredScopes.every(scopeData => hasScopeInScopeData({
        scopeData,
        scopes,
    }));
    return hasScopes;
};
exports.getErrorsInLocal = (error) => {
    const statusCodes = [error.status, error.code, error.statusCode];
    if (statusCodes.includes(401)) {
        return {
            error: 'Unauthorized',
            code: 401,
        };
    }
    if (error.name === 'MongoError' && error.code === 11000) {
        return {
            message: 'Duplicated email, name or phone',
            error: 'Bad Request',
            code: 400,
        };
    }
    if (statusCodes.includes(403)) {
        return {
            message: error.message || error.name,
            code: 403,
            error: 'Forbidden',
        };
    }
    if (statusCodes.includes(404)) {
        return {
            message: error.message || error.name,
            code: 404,
            error: 'Not Found',
        };
    }
    if (statusCodes.includes(400)) {
        return {
            message: error.message || error.name,
            error: 'Bad Request',
            code: 400,
        };
    }
    return {
        message: `${error.message} - ${error.stack}`,
        code: 500,
        error: 'Internal Server Error',
    };
};
exports.getErrorsInProd = (error) => {
    const statusCodes = [error.status, error.code, error.statusCode];
    if (statusCodes.includes(401)) {
        return {
            error: 'Unauthorized',
            code: 401,
        };
    }
    if (error.name === 'MongoError' && error.code === 11000) {
        return {
            message: 'Duplicated email, name or phone',
            code: 400,
            error: 'Bad Request',
        };
    }
    if (error.name === 'ValidationError' && statusCodes.includes(403)) {
        return {
            message: 'No permissions',
            code: 403,
            error: 'Forbidden',
        };
    }
    if (statusCodes.includes(404)) {
        return {
            message: error.message || error.name,
            code: 404,
            error: 'Not Found',
        };
    }
    if (statusCodes.includes(400)) {
        return {
            message: error.message || error.name,
            code: 400,
            error: 'Bad Request',
        };
    }
    return {
        message: 'Something wrong happens',
        code: 500,
        error: 'Internal Server Error',
    };
};
exports.capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
};
//# sourceMappingURL=helpers.js.map