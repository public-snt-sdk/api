import { PipeTransform, ArgumentMetadata } from '@nestjs/common';
export declare class ValidationPipe implements PipeTransform<object> {
    transform(value: object, { metatype }: ArgumentMetadata): Promise<any>;
    private toValidate;
}
