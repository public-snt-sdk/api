"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FirebaseService = void 0;
const firebase_admin_1 = require("firebase-admin");
class FirebaseService {
    constructor(clientEmail, privateKey, projectId) {
        if (!firebase_admin_1.apps.length) {
            firebase_admin_1.initializeApp({
                credential: firebase_admin_1.credential.cert({
                    clientEmail,
                    privateKey,
                    projectId,
                }),
            });
        }
    }
    sendMessageToDevice({ tokens, notification, data, options = { priority: 'normal', timeToLive: 12 }, }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const payload = {
                    notification: Object.assign({}, notification),
                    data: Object.assign({}, data),
                };
                yield firebase_admin_1.messaging().sendToDevice(tokens, payload, options);
                return true;
            }
            catch (error) {
                console.log(error);
                return Promise.reject(error);
            }
        });
    }
    sendMessageToTopic({ topic, notification, data, options = { priority: 'normal', timeToLive: 12 }, }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const payload = {
                    notification: Object.assign({}, notification),
                    data: Object.assign({}, data),
                };
                yield firebase_admin_1.messaging().sendToTopic(topic, payload, options);
                return true;
            }
            catch (error) {
                console.log(error);
                return Promise.reject(error);
            }
        });
    }
    subscribeTokenToTopic({ tokens, topic }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!Array.isArray(tokens) || !tokens.length) {
                    return true;
                }
                yield firebase_admin_1.messaging().subscribeToTopic(tokens, topic);
                return true;
            }
            catch (error) {
                console.log(error);
                return Promise.reject(error);
            }
        });
    }
    unsubscribeTokenFromTopic({ tokens, topic }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!Array.isArray(tokens) || !tokens.length) {
                    return true;
                }
                yield firebase_admin_1.messaging().unsubscribeFromTopic(tokens, topic);
                return true;
            }
            catch (error) {
                console.log(error);
                return Promise.reject(error);
            }
        });
    }
}
exports.FirebaseService = FirebaseService;
//# sourceMappingURL=firebase.js.map