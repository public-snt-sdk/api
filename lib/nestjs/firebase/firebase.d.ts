import { SendMessageToDeviceService, SendMessageToTopicService, SubscribeTokenToTopicService, UnsubscribeTokenFromTopicService } from './firebase.interface';
export declare class FirebaseService {
    constructor(clientEmail: string, privateKey: string, projectId: string);
    sendMessageToDevice({ tokens, notification, data, options, }: SendMessageToDeviceService): Promise<boolean>;
    sendMessageToTopic({ topic, notification, data, options, }: SendMessageToTopicService): Promise<boolean>;
    subscribeTokenToTopic({ tokens, topic }: SubscribeTokenToTopicService): Promise<boolean>;
    unsubscribeTokenFromTopic({ tokens, topic }: UnsubscribeTokenFromTopicService): Promise<boolean>;
}
