import { messaging } from 'firebase-admin';
interface NotificationData {
    [key: string]: string;
}
interface FirebaseData {
    [key: string]: string;
}
export interface SendMessageToTopicService {
    topic: string;
    notification: NotificationData;
    data: FirebaseData;
    options?: messaging.MessagingOptions;
}
export interface SendMessageToDeviceService {
    tokens: string[];
    notification: NotificationData;
    data: FirebaseData;
    options?: messaging.MessagingOptions;
}
export interface SubscribeTokenToTopicService {
    tokens: string[];
    topic: string;
}
export interface UnsubscribeTokenFromTopicService {
    tokens: string[];
    topic: string;
}
export {};
