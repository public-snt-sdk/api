import { ErrorObject } from './exception';
interface CheckScope {
    scopes: string[];
    requiredScopes: string[][];
}
export declare const checkScope: ({ scopes, requiredScopes }: CheckScope) => boolean;
export declare const getErrorsInLocal: (error: ErrorObject) => {
    error: string;
    code: number;
    message?: undefined;
} | {
    message: string | number | undefined;
    code: number;
    error: string;
};
export declare const getErrorsInProd: (error: ErrorObject) => {
    error: string;
    code: number;
    message?: undefined;
} | {
    message: string | number | undefined;
    code: number;
    error: string;
};
export declare const capitalizeFirstLetter: (str: string) => string;
export {};
