import { ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
export interface ErrorObject {
    [key: string]: string | number | undefined;
}
interface GetErrorResult {
    message?: any;
    error: string;
    code: number;
}
declare type GetErrors = (error: ErrorObject) => GetErrorResult;
export declare class HttpExceptionFilter implements ExceptionFilter {
    constructor(getErrors?: GetErrors);
    nodeEnv: string | undefined;
    getErrors: GetErrors;
    catch(exception: ErrorObject, host: ArgumentsHost): Response<any, Record<string, any>>;
}
export {};
