"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Scopes = void 0;
const helpers_1 = require("./helpers");
class Scopes {
    constructor(requiredScopes, personalKey = 'personalScopes', firstGroupKey = 'merchant', secondGroupKey = 'store', getRolesKey = 'roles', getPersonalScopes, getFirstGroupScopes, getSecondGroupScopes) {
        this.requiredScopes = requiredScopes;
        this.getPersonalScopes = (scopes, requiredScopes) => {
            if (!scopes || scopes.length === 0) {
                return false;
            }
            const hasPersonalScopes = helpers_1.checkScope({
                scopes,
                requiredScopes,
            });
            return hasPersonalScopes;
        };
        this.getFirstGroupScopes = (accessWithScopes, requiredScopes) => {
            let hasFirstGroupScopes = false;
            const firstGroupIDs = [];
            if (!accessWithScopes || !Object.keys(accessWithScopes).length) {
                return {
                    hasFirstGroupScopes,
                    firstGroupIDs,
                };
            }
            for (const access in accessWithScopes) {
                if (!accessWithScopes[access]) {
                    continue;
                }
                hasFirstGroupScopes = helpers_1.checkScope({
                    scopes: accessWithScopes[access],
                    requiredScopes,
                });
                if (!hasFirstGroupScopes) {
                    continue;
                }
                firstGroupIDs.push(access);
            }
            return {
                hasFirstGroupScopes: firstGroupIDs.length ? true : false,
                firstGroupIDs,
            };
        };
        this.getSecondGroupScopes = (accessWithScopes, requiredScopes) => {
            let hasSecondGroupScopes = false;
            const secondGroupIDs = [];
            if (!accessWithScopes || !Object.keys(accessWithScopes).length) {
                return {
                    hasSecondGroupScopes,
                    secondGroupIDs,
                };
            }
            for (const access in accessWithScopes) {
                if (!accessWithScopes[access]) {
                    continue;
                }
                hasSecondGroupScopes = helpers_1.checkScope({
                    scopes: accessWithScopes[access],
                    requiredScopes,
                });
                if (!hasSecondGroupScopes) {
                    continue;
                }
                secondGroupIDs.push(access);
            }
            return {
                hasSecondGroupScopes: secondGroupIDs.length ? true : false,
                secondGroupIDs,
            };
        };
        this.getRolesKey = getRolesKey;
        this.personalKey = personalKey;
        this.firstGroupKey = firstGroupKey;
        this.secondGroupKey = secondGroupKey;
        if (getPersonalScopes)
            this.getPersonalScopes = getPersonalScopes;
        if (getFirstGroupScopes)
            this.getFirstGroupScopes = getFirstGroupScopes;
        if (getSecondGroupScopes)
            this.getSecondGroupScopes = getSecondGroupScopes;
    }
    canActivate(context) {
        const requiredScopes = this.requiredScopes;
        const req = context.switchToHttp().getRequest();
        const user = req.user;
        if (!requiredScopes || !requiredScopes.length) {
            req.user.isPublic = true;
            return true;
        }
        if (!user) {
            return false;
        }
        const personalScopes = user[`${this.personalKey}Scopes`] || [];
        const firstGroupScopes = user[`${this.firstGroupKey}Scopes`] || [];
        const secondGroupScopes = user[`${this.secondGroupKey}Scopes`] || [];
        const roles = user[this.getRolesKey];
        if (roles.includes('ADMIN')) {
            req.user.isAdmin = true;
            return true;
        }
        const hasScopes = personalScopes.length
            || firstGroupScopes.length
            || secondGroupScopes.length;
        if (!hasScopes) {
            return false;
        }
        const hasPersonalScopes = this.getPersonalScopes(personalScopes, requiredScopes);
        const { hasFirstGroupScopes, firstGroupIDs } = this.getFirstGroupScopes(firstGroupScopes, requiredScopes);
        const { hasSecondGroupScopes, secondGroupIDs } = this.getSecondGroupScopes(secondGroupScopes, requiredScopes);
        const capitalizeFirstLetterForFirstGroupKey = helpers_1.capitalizeFirstLetter(this.firstGroupKey);
        const capitalizeFirstLetterForSecondGroupKey = helpers_1.capitalizeFirstLetter(this.secondGroupKey);
        req.user.hasPersonalScopes = hasPersonalScopes;
        req.user[`has${capitalizeFirstLetterForFirstGroupKey}Scopes`] = hasFirstGroupScopes;
        req.user[`has${capitalizeFirstLetterForSecondGroupKey}Scopes`] = hasSecondGroupScopes;
        req.user[`${this.firstGroupKey}IDs`] = firstGroupIDs;
        req.user[`${this.secondGroupKey}IDs`] = secondGroupIDs;
        if (hasPersonalScopes || hasFirstGroupScopes || hasSecondGroupScopes) {
            return true;
        }
        return false;
    }
}
exports.Scopes = Scopes;
//# sourceMappingURL=authz.js.map