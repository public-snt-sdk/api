"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const helpers_1 = require("./helpers");
class HttpExceptionFilter {
    constructor(getErrors) {
        this.nodeEnv = process.env.NODE_ENV;
        this.getErrors = process.env.NODE_ENV !== 'production'
            ? helpers_1.getErrorsInLocal
            : helpers_1.getErrorsInProd;
        if (getErrors)
            this.getErrors = getErrors;
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const nodeEnv = process.env.NODE_ENV || 'local';
        const shouldLogRequestEnvironments = ['production', 'dev', 'uat', 'staging'];
        if (shouldLogRequestEnvironments.includes(nodeEnv)) {
            console.log('____REQUEST____', request);
        }
        const exceptionResponse = exception.response || exception.message;
        const errorObject = typeof exceptionResponse === 'object' ?
            exceptionResponse :
            exception;
        const { code, message, error } = this.getErrors(errorObject);
        if (code === 401) {
            return response
                .status(code)
                .json({
                error,
                statusCode: code,
            });
        }
        if (!message) {
            return response
                .status(400)
                .json({
                statusCode: 400,
                message: 'need to update error message into errors parameter',
            });
        }
        return response
            .status(code)
            .json({
            error,
            statusCode: code,
            message: message || errorObject.message || exception.message,
        });
    }
}
exports.HttpExceptionFilter = HttpExceptionFilter;
//# sourceMappingURL=exception.js.map