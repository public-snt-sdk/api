import { CanActivate, ExecutionContext } from '@nestjs/common';
interface GetFirstGroupScopesResult {
    hasFirstGroupScopes: boolean;
    firstGroupIDs: string[];
}
interface GetSecondGroupScopesResult {
    hasSecondGroupScopes: boolean;
    secondGroupIDs: string[];
}
interface AccessWithScopes {
    [key: string]: string[];
}
declare type GetPersonalScope = (scopes: string[], requiredScopes: string[][]) => boolean;
declare type GetFirstGroupScopes = (accessWithScopes: AccessWithScopes, requiredScopes: string[][]) => GetFirstGroupScopesResult;
declare type GetSecondGroupScopes = (accessWithScopes: AccessWithScopes, requiredScopes: string[][]) => GetSecondGroupScopesResult;
export declare class Scopes implements CanActivate {
    private readonly requiredScopes;
    constructor(requiredScopes: string[][], personalKey?: string, firstGroupKey?: string, secondGroupKey?: string, getRolesKey?: string, getPersonalScopes?: GetPersonalScope, getFirstGroupScopes?: GetFirstGroupScopes, getSecondGroupScopes?: GetSecondGroupScopes);
    getRolesKey: string;
    personalKey: string;
    firstGroupKey: string;
    secondGroupKey: string;
    getPersonalScopes: GetPersonalScope;
    getFirstGroupScopes: GetFirstGroupScopes;
    getSecondGroupScopes: GetSecondGroupScopes;
    canActivate(context: ExecutionContext): boolean;
}
export {};
