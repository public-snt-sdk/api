import { TrackingSettings, MailSettings, ASMOptions, MailContent } from '@sendgrid/helpers/classes/mail';
import { AttachmentData } from '@sendgrid/helpers/classes/attachment';
import { PersonalizationData } from '@sendgrid/helpers/classes/personalization';
import { EmailData } from '@sendgrid/helpers/classes/email-address';
export interface SendEmail {
    mail: {
        to?: EmailData | EmailData[];
        cc?: EmailData | EmailData[];
        bcc?: EmailData | EmailData[];
        replyTo?: EmailData;
        sendAt?: number;
        subject?: string;
        text?: string;
        html: string;
        content?: MailContent[];
        templateId?: string;
        personalizations?: PersonalizationData[];
        attachments?: AttachmentData[];
        ipPoolName?: string;
        batchId?: string;
        sections?: {
            [key: string]: string;
        };
        headers?: {
            [key: string]: string;
        };
        categories?: string[];
        category?: string;
        customArgs?: {
            [key: string]: any;
        };
        asm?: ASMOptions;
        mailSettings?: MailSettings;
        trackingSettings?: TrackingSettings;
        substitutions?: {
            [key: string]: string;
        };
        substitutionWrappers?: string[];
        isMultiple?: boolean;
        dynamicTemplateData?: {
            [key: string]: any;
        };
        hideWarnings?: boolean;
    };
}
