"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendGrid = void 0;
const sgMail = require("@sendgrid/mail");
class SendGrid {
    constructor(apiKey, senderDefault) {
        this.senderDefault = senderDefault;
        sgMail.setApiKey(apiKey);
    }
    sendMailSDK({ mail }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const mailSend = Object.assign({ from: this.senderDefault }, mail);
                const send = yield sgMail.send(mailSend);
                return send;
            }
            catch (error) {
                return Promise.reject(error);
            }
        });
    }
}
exports.SendGrid = SendGrid;
//# sourceMappingURL=send_grid.js.map