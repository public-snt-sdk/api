import * as sgMail from '@sendgrid/mail';
import { SendEmail } from './send_grid.interface';
export declare class SendGrid {
    constructor(apiKey: string, senderDefault: string);
    senderDefault: string;
    sendMailSDK({ mail }: SendEmail): Promise<[sgMail.ClientResponse, {}]>;
}
