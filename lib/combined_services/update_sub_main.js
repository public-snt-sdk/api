"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.classifyCases = exports.runFindManyMainSubs = exports.runCreateUpdate = exports.runDeleteManyMainSubs = exports.runCreateManySubs = exports.runDeleteAllMainSubs = exports.updateMainAndSubs = void 0;
exports.updateMainAndSubs = ({ mainSubsService, findManyMainSubsQuery, mainsService, updateMainPayload, subsService, updateManyMainSubPayload, updateManySubPayload, mapMainSubs, mainID, mainIDKey, subIDKey, credentials, }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let updatedMain;
        if (updateMainPayload && (mainsService === null || mainsService === void 0 ? void 0 : mainsService.updateOne) && typeof mainIDKey === 'string') {
            const query = {};
            query[mainIDKey] = mainID;
            updatedMain = yield mainsService.updateOne({
                query,
                updateOneData: updateMainPayload,
                credentials,
            });
        }
        if (!updateManyMainSubPayload) {
            return {
                updatedMain,
            };
        }
        const currentMainSubs = yield exports.runFindManyMainSubs({
            findManyMainSubsQuery, mainIDKey, mainSubsService, credentials,
        });
        const shouldNotUpdateMainSubs = yield exports.runDeleteAllMainSubs({
            updateManyMainSubPayload, mainIDKey,
            mainID, subIDKey, currentMainSubs, mainSubsService, credentials,
        });
        if (shouldNotUpdateMainSubs) {
            return {
                updatedMain,
            };
        }
        const toUpdateMainSubs = [];
        const { comparingObject, existingSubs, toCreateMainSubs, toCreateSubs, toUpdateSubs, } = exports.classifyCases({
            currentMainSubs, subIDKey, updateManyMainSubPayload, updateManySubPayload,
        });
        const createdManySubs = yield exports.runCreateManySubs({ toCreateSubs, toCreateMainSubs, subsService, subIDKey, credentials });
        if (createdManySubs === null || createdManySubs === void 0 ? void 0 : createdManySubs.length) {
            for (const [i, sub] of createdManySubs.entries()) {
                toCreateMainSubs[i][subIDKey] = sub[subIDKey];
            }
        }
        for (const mainSub of existingSubs) {
            const ID = mainSub[subIDKey];
            if (typeof ID !== 'string' || !comparingObject[ID]) {
                toCreateMainSubs.push(mainSub);
                continue;
            }
            toUpdateMainSubs.push(mainSub);
            delete comparingObject[ID];
        }
        const toRemoveMainSubs = Object.keys(comparingObject);
        const mainSubPayload = mapMainSubs({
            mainID,
            data: toCreateMainSubs,
            credentials,
        });
        const deleteManyMainSubsResult = yield exports.runDeleteManyMainSubs({ toRemoveMainSubs, mainIDKey, mainID, subIDKey, mainSubsService, credentials });
        const { createManySubsResult, createManyMainSubsResult, updateManySubsResult, updateManyMainSubsResult } = yield exports.runCreateUpdate({ subsService, mainSubsService, toCreateSubs, mainSubPayload,
            toUpdateSubs, toUpdateMainSubs, mainIDKey, mainID, credentials });
        return {
            updatedMain,
            createManySubsResult,
            createManyMainSubsResult,
            updateManySubsResult,
            updateManyMainSubsResult,
            findManyMainSubsResult: currentMainSubs,
            deleteManyMainSubsResult,
        };
    }
    catch (error) {
        throw error;
    }
});
exports.runDeleteAllMainSubs = ({ updateManyMainSubPayload, mainIDKey, mainID, subIDKey, currentMainSubs, mainSubsService, credentials }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const shouldNotUpdateMainSubs = !(updateManyMainSubPayload === null || updateManyMainSubPayload === void 0 ? void 0 : updateManyMainSubPayload.length);
        if (shouldNotUpdateMainSubs && !(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.deleteMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs deleteMany function`,
            };
        }
        if (shouldNotUpdateMainSubs && (mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.deleteMany)
            && typeof mainIDKey === 'string'
            && typeof subIDKey === 'string') {
            const toDeleteIDs = currentMainSubs === null || currentMainSubs === void 0 ? void 0 : currentMainSubs.map((each) => each[subIDKey]);
            const toRemoveMainSubsQuery = {};
            toRemoveMainSubsQuery[mainIDKey] = mainID;
            toRemoveMainSubsQuery[subIDKey] = toDeleteIDs;
            yield mainSubsService.deleteMany({
                query: toRemoveMainSubsQuery,
                credentials,
            });
        }
        return shouldNotUpdateMainSubs;
    }
    catch (error) {
        throw error;
    }
});
exports.runCreateManySubs = ({ toCreateSubs, toCreateMainSubs, subsService, subIDKey, credentials }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const shouldCreateSubs = toCreateSubs === null || toCreateSubs === void 0 ? void 0 : toCreateSubs.length;
        const validCreateSubsAndCreateMainSubs = (toCreateMainSubs === null || toCreateMainSubs === void 0 ? void 0 : toCreateMainSubs.length) === toCreateSubs.length;
        if (!validCreateSubsAndCreateMainSubs) {
            throw {
                code: 400,
                name: `if you want created sub, please check input data <subsPayload.length should equal MainSubPayload(not ${subIDKey}).length>`,
            };
        }
        if (shouldCreateSubs && !(subsService === null || subsService === void 0 ? void 0 : subsService.createMany)) {
            throw {
                code: 400,
                name: `subsService needs createMany function`,
            };
        }
        let createdManySubs;
        if (shouldCreateSubs && (subsService === null || subsService === void 0 ? void 0 : subsService.createMany)) {
            createdManySubs = yield subsService.createMany({
                data: toCreateSubs,
                credentials,
            });
        }
        return createdManySubs;
    }
    catch (error) {
        throw error;
    }
});
exports.runDeleteManyMainSubs = ({ toRemoveMainSubs, mainIDKey, mainID, subIDKey, mainSubsService, credentials }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let deleteManyMainSubsResult;
        if ((toRemoveMainSubs === null || toRemoveMainSubs === void 0 ? void 0 : toRemoveMainSubs.length) && !(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.deleteMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs deleteMany function`,
            };
        }
        if ((toRemoveMainSubs === null || toRemoveMainSubs === void 0 ? void 0 : toRemoveMainSubs.length) && (mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.deleteMany)
            && typeof mainIDKey === 'string'
            && typeof subIDKey === 'string') {
            const toRemoveMainSubsQuery = {};
            toRemoveMainSubsQuery[mainIDKey] = mainID;
            toRemoveMainSubsQuery[subIDKey] = toRemoveMainSubs;
            deleteManyMainSubsResult = yield mainSubsService.deleteMany({
                query: toRemoveMainSubsQuery,
                credentials,
            });
        }
        return deleteManyMainSubsResult;
    }
    catch (error) {
        throw error;
    }
});
exports.runCreateUpdate = ({ subsService, mainSubsService, toCreateSubs, mainSubPayload, toUpdateSubs, toUpdateMainSubs, mainIDKey, mainID, credentials }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!(subsService === null || subsService === void 0 ? void 0 : subsService.createMany)) {
            throw {
                code: 400,
                name: `subsService needs createMany function`,
            };
        }
        if (!(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.createMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs createMany function`,
            };
        }
        if (!(subsService === null || subsService === void 0 ? void 0 : subsService.updateMany)) {
            throw {
                code: 400,
                name: `subsService needs updateMany function`,
            };
        }
        if (!(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.updateMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs updateMany function`,
            };
        }
        if (typeof mainIDKey !== 'string') {
            throw {
                code: 400,
                name: `mainIDKey must be typeof string`,
            };
        }
        const [createManySubsResult, createManyMainSubsResult, updateManySubsResult, updateManyMainSubsResult] = yield Promise.all([
            subsService.createMany(toCreateSubs),
            mainSubsService.createMany(mainSubPayload),
            subsService.updateMany({
                data: toUpdateSubs,
                credentials,
            }),
            mainSubsService.updateMany({
                data: toUpdateMainSubs,
                [mainIDKey]: mainID,
                credentials,
            }),
        ]);
        return {
            createManySubsResult,
            createManyMainSubsResult,
            updateManySubsResult,
            updateManyMainSubsResult,
        };
    }
    catch (error) {
        throw error;
    }
});
exports.runFindManyMainSubs = ({ findManyMainSubsQuery, mainIDKey, mainSubsService, credentials, }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const hasMainIdFiltering = !findManyMainSubsQuery || !findManyMainSubsQuery[mainIDKey];
        if (hasMainIdFiltering) {
            throw {
                code: 400,
                name: `findManyMainSubsQuery needs filter by ${mainIDKey}`,
            };
        }
        if (!(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.findMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs findMany function`,
            };
        }
        const { list: currentMainSubs } = yield mainSubsService.findMany({
            query: findManyMainSubsQuery,
            credentials,
        });
        return currentMainSubs;
    }
    catch (error) {
        throw error;
    }
});
exports.classifyCases = ({ currentMainSubs, subIDKey, updateManyMainSubPayload, updateManySubPayload, }) => {
    const toCreateMainSubs = [];
    const existingSubs = [];
    const toCreateSubs = [];
    const toUpdateSubs = [];
    const comparingObject = {};
    for (const mainSub of currentMainSubs) {
        const idKey = mainSub[subIDKey];
        if (!idKey || typeof idKey !== 'string')
            continue;
        comparingObject[idKey] = mainSub;
    }
    for (const mainSub of updateManyMainSubPayload) {
        const idKey = mainSub[subIDKey];
        if (idKey) {
            existingSubs.push(mainSub);
            continue;
        }
        toCreateMainSubs.push(mainSub);
    }
    if (updateManySubPayload === null || updateManySubPayload === void 0 ? void 0 : updateManySubPayload.length) {
        for (const subPayload of updateManySubPayload) {
            if (subPayload[subIDKey]) {
                toUpdateSubs.push(subPayload);
                continue;
            }
            toCreateSubs.push(subPayload);
        }
    }
    return {
        comparingObject,
        existingSubs,
        toCreateMainSubs,
        toCreateSubs,
        toUpdateSubs,
    };
};
//# sourceMappingURL=update_sub_main.js.map