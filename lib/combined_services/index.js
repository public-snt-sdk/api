"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var create_sub_main_1 = require("./create_sub_main");
Object.defineProperty(exports, "createMainAndSubs", { enumerable: true, get: function () { return create_sub_main_1.createMainAndSubs; } });
var update_sub_main_1 = require("./update_sub_main");
Object.defineProperty(exports, "updateMainAndSubs", { enumerable: true, get: function () { return update_sub_main_1.updateMainAndSubs; } });
//# sourceMappingURL=index.js.map