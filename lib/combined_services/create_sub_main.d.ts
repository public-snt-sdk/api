import { ServiceInterface } from './common.type';
interface CreateMainAndSubsService<SubIDKey, MainInterface, MainSubInterface, SubInterface> {
    mainsService: ServiceInterface;
    mainPayload: MainInterface;
    subsService: ServiceInterface;
    subsPayload?: [SubInterface] | [];
    mainSubsService: ServiceInterface;
    MainSubPayload?: [MainSubInterface] | [];
    mapMainSubs: Function;
    subIDKey: SubIDKey;
    credentials?: object;
}
export declare const createMainAndSubs: <SubIDKey extends keyof MainSubInterface, MainInterface, MainSubInterface extends SubInterface, SubInterface>({ mainsService, mainPayload, subsService, subsPayload, mainSubsService, MainSubPayload, mapMainSubs, subIDKey, credentials, }: CreateMainAndSubsService<SubIDKey, MainInterface, MainSubInterface, SubInterface>) => Promise<{
    createdMain: any;
    createdManySubs?: undefined;
    createdMainSubs?: undefined;
} | {
    createdMain: any;
    createdManySubs: any;
    createdMainSubs: any;
}>;
export {};
