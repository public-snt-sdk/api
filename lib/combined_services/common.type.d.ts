export interface ServiceInterface {
    createOne?: Function;
    createMany?: Function;
    findOne?: Function;
    findMany?: Function;
    deleteOne?: Function;
    deleteMany?: Function;
    updateOne?: Function;
    updateMany?: Function;
}
