import { ServiceInterface } from './common.type';
interface UpdateMainAndSubs<SubIDKey, MainIDKey, MainInterface, MainSubInterface, SubInterface, ManyMainSubsQuery> {
    mainSubsService: ServiceInterface;
    findManyMainSubsQuery: ManyMainSubsQuery;
    mainsService: ServiceInterface;
    updateMainPayload: MainInterface;
    updateManyMainSubPayload?: [MainSubInterface] | [];
    subsService: ServiceInterface;
    updateManySubPayload?: SubInterface[];
    mapMainSubs: Function;
    mainID: string;
    mainIDKey: MainIDKey;
    subIDKey: SubIDKey;
    credentials: object;
}
interface RunDeleteAllMainSubs<MainIDKey, SubIDKey, MainSubInterface> {
    updateManyMainSubPayload: MainSubInterface[];
    mainIDKey: MainIDKey;
    mainID: string;
    subIDKey: SubIDKey;
    currentMainSubs: MainSubInterface[];
    mainSubsService: ServiceInterface;
    credentials: object;
}
export declare const updateMainAndSubs: <SubIDKey extends keyof SubInterface, MainIDKey extends keyof MainSubInterface, MainInterface, MainSubInterface extends SubInterface, SubInterface, ManyMainSubsQuery extends MainSubInterface>({ mainSubsService, findManyMainSubsQuery, mainsService, updateMainPayload, subsService, updateManyMainSubPayload, updateManySubPayload, mapMainSubs, mainID, mainIDKey, subIDKey, credentials, }: UpdateMainAndSubs<SubIDKey, MainIDKey, MainInterface, MainSubInterface, SubInterface, ManyMainSubsQuery>) => Promise<{
    updatedMain: any;
    createManySubsResult?: undefined;
    createManyMainSubsResult?: undefined;
    updateManySubsResult?: undefined;
    updateManyMainSubsResult?: undefined;
    findManyMainSubsResult?: undefined;
    deleteManyMainSubsResult?: undefined;
} | {
    updatedMain: any;
    createManySubsResult: any;
    createManyMainSubsResult: any;
    updateManySubsResult: any;
    updateManyMainSubsResult: any;
    findManyMainSubsResult: any;
    deleteManyMainSubsResult: any;
}>;
export declare const runDeleteAllMainSubs: <MainIDKey, SubIDKey extends keyof MainSubInterface, MainSubInterface>({ updateManyMainSubPayload, mainIDKey, mainID, subIDKey, currentMainSubs, mainSubsService, credentials }: RunDeleteAllMainSubs<MainIDKey, SubIDKey, MainSubInterface>) => Promise<boolean>;
interface RunCreateManySubsService<MainSubInterface, SubInterface, SubIDKey> {
    toCreateSubs: SubInterface[];
    toCreateMainSubs: MainSubInterface[];
    subsService: ServiceInterface;
    subIDKey: SubIDKey;
    credentials: object;
}
export declare const runCreateManySubs: <MainSubInterface, SubInterface, SubIDKey extends keyof SubInterface>({ toCreateSubs, toCreateMainSubs, subsService, subIDKey, credentials }: RunCreateManySubsService<MainSubInterface, SubInterface, SubIDKey>) => Promise<any>;
interface RunDeleteManyMainSubsService<MainIDKey, SubIDKey> {
    toRemoveMainSubs: string[];
    mainIDKey: MainIDKey;
    mainID: string;
    subIDKey: SubIDKey;
    mainSubsService: ServiceInterface;
    credentials: object;
}
export declare const runDeleteManyMainSubs: <MainIDKey, SubIDKey>({ toRemoveMainSubs, mainIDKey, mainID, subIDKey, mainSubsService, credentials }: RunDeleteManyMainSubsService<MainIDKey, SubIDKey>) => Promise<any>;
interface RunCreateUpdateService<MainInterface, MainSubInterface, SubInterface, MainIDKey> {
    subsService: ServiceInterface;
    mainSubsService: ServiceInterface;
    toCreateSubs: SubInterface[];
    mainSubPayload: MainInterface;
    toUpdateSubs: SubInterface[];
    toUpdateMainSubs: MainSubInterface[];
    mainIDKey: MainIDKey;
    mainID: string;
    credentials: object;
}
export declare const runCreateUpdate: <MainInterface, MainSubInterface, SubInterface, MainIDKey>({ subsService, mainSubsService, toCreateSubs, mainSubPayload, toUpdateSubs, toUpdateMainSubs, mainIDKey, mainID, credentials }: RunCreateUpdateService<MainInterface, MainSubInterface, SubInterface, MainIDKey>) => Promise<{
    createManySubsResult: any;
    createManyMainSubsResult: any;
    updateManySubsResult: any;
    updateManyMainSubsResult: any;
}>;
interface RunFindManyMainSubsInterface<MainIDKey, MainSubInterface, ManyMainSubsQuery> {
    findManyMainSubsQuery: ManyMainSubsQuery;
    mainIDKey: MainIDKey;
    mainSubsService: ServiceInterface;
    credentials: object;
}
export declare const runFindManyMainSubs: <MainIDKey extends keyof MainSubInterface, MainSubInterface, ManyMainSubsQuery extends MainSubInterface>({ findManyMainSubsQuery, mainIDKey, mainSubsService, credentials, }: RunFindManyMainSubsInterface<MainIDKey, MainSubInterface, ManyMainSubsQuery>) => Promise<any>;
interface GetDataService<SubIDKey, MainSubInterface, SubInterface> {
    currentMainSubs: MainSubInterface[];
    subIDKey: SubIDKey;
    updateManyMainSubPayload: MainSubInterface[];
    updateManySubPayload?: SubInterface[];
}
export declare const classifyCases: <SubIDKey extends keyof SubInterface, MainSubInterface extends SubInterface, SubInterface>({ currentMainSubs, subIDKey, updateManyMainSubPayload, updateManySubPayload, }: GetDataService<SubIDKey, MainSubInterface, SubInterface>) => {
    comparingObject: {
        [key: string]: MainSubInterface;
    };
    existingSubs: MainSubInterface[];
    toCreateMainSubs: MainSubInterface[];
    toCreateSubs: SubInterface[];
    toUpdateSubs: SubInterface[];
};
export {};
