"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMainAndSubs = void 0;
exports.createMainAndSubs = ({ mainsService, mainPayload, subsService, subsPayload = [], mainSubsService, MainSubPayload = [], mapMainSubs, subIDKey, credentials, }) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const existingSubs = [];
        const toCreateMainSubs = [];
        for (const mainSub of MainSubPayload) {
            if (mainSub[subIDKey]) {
                existingSubs.push(mainSub);
                continue;
            }
            toCreateMainSubs.push(mainSub);
        }
        const shouldCreateSubs = (toCreateMainSubs === null || toCreateMainSubs === void 0 ? void 0 : toCreateMainSubs.length) && (subsPayload === null || subsPayload === void 0 ? void 0 : subsPayload.length);
        const validCreateSubsAndCreateMainSubs = (toCreateMainSubs === null || toCreateMainSubs === void 0 ? void 0 : toCreateMainSubs.length) === (subsPayload === null || subsPayload === void 0 ? void 0 : subsPayload.length);
        if (!validCreateSubsAndCreateMainSubs) {
            throw {
                code: 400,
                name: `if you want created sub, please check input data <subsPayload.length should equal MainSubPayload(not ${subIDKey}).length>`,
            };
        }
        if (!(mainsService === null || mainsService === void 0 ? void 0 : mainsService.createOne)) {
            throw {
                code: 400,
                name: `mainService needs create one function`,
            };
        }
        if (!(subsService === null || subsService === void 0 ? void 0 : subsService.createMany)) {
            throw {
                code: 400,
                name: `subsService needs create many function`,
            };
        }
        const [createdMain, createdManySubs] = yield Promise.all([
            mainsService.createOne({
                data: mainPayload,
                credentials,
            }),
            shouldCreateSubs ? subsService.createMany({
                data: subsPayload,
                credentials,
            }) : null,
        ]);
        if (createdManySubs === null || createdManySubs === void 0 ? void 0 : createdManySubs.length) {
            for (const [i, sub] of createdManySubs.entries()) {
                toCreateMainSubs[i][subIDKey] = sub[subIDKey];
            }
            Array.prototype.push.apply(existingSubs, toCreateMainSubs);
        }
        const mainSubPayload = mapMainSubs({
            createdMain,
            existingSubs,
            credentials,
        });
        const shouldNotCreateMainSubs = !mainSubPayload || !(existingSubs === null || existingSubs === void 0 ? void 0 : existingSubs.length);
        if (shouldNotCreateMainSubs) {
            return {
                createdMain,
            };
        }
        if (!(mainSubsService === null || mainSubsService === void 0 ? void 0 : mainSubsService.createMany)) {
            throw {
                code: 400,
                name: `mainSubsService needs create many function`,
            };
        }
        const createdMainSubs = yield mainSubsService.createMany(mainSubPayload);
        return {
            createdMain,
            createdManySubs,
            createdMainSubs,
        };
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=create_sub_main.js.map