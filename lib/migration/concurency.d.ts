export declare const getTasks: (tasks: any) => () => any;
export declare const runConcurrency: (numConcurrentTasks: number, promises: Function[]) => Promise<void>;
