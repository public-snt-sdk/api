"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.runConcurrency = exports.getTasks = void 0;
const EventEmitter = require('events');
exports.getTasks = (tasks) => {
    return () => {
        const task = tasks.shift();
        if (!task) {
            return;
        }
        return task;
    };
};
exports.runConcurrency = (numConcurrentTasks, promises) => __awaiter(void 0, void 0, void 0, function* () {
    const totalTasks = promises.length || 0;
    const setNumConcurrentTasks = Math.min(numConcurrentTasks, totalTasks);
    const getTask = exports.getTasks(promises);
    const processingEmitter = new EventEmitter();
    let numberOfDoneTasks = 0;
    const waitForAllDone = () => new Promise((resolve, reject) => {
        if (numberOfDoneTasks === totalTasks) {
            resolve();
        }
        processingEmitter.once('allDone', (_) => {
            resolve();
        });
    });
    processingEmitter.on('oneTaskDone', () => {
        numberOfDoneTasks++;
        const task = getTask();
        if (!task && numberOfDoneTasks < totalTasks) {
            return;
        }
        if (numberOfDoneTasks === totalTasks) {
            return processingEmitter.emit('allDone');
        }
        task()
            .then(() => processingEmitter.emit('oneTaskDone'));
    });
    try {
        const initialTasks = [];
        for (let i = 1; i <= setNumConcurrentTasks; i++) {
            initialTasks.push(getTask()()
                .then(() => processingEmitter.emit('oneTaskDone')));
        }
        yield Promise.all(initialTasks);
        yield waitForAllDone();
    }
    catch (error) {
        Promise.reject(error);
    }
});
//# sourceMappingURL=concurency.js.map