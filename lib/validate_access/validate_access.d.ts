export interface ValidateAccessToSingle<T> {
    data: T;
    credentials: Credentials;
}
export interface ValidateAccessToList<T> {
    data: T[];
    credentials: Credentials;
}
interface ValidateFirstGroup<T> {
    data: T;
    firstGroupIDs: string[];
    hasFirstGroupScopes: boolean;
}
interface ValidateSecondGroup<T> {
    data: T;
    secondGroupIDs: string[];
    hasSecondGroupScopes: boolean;
}
interface ValidateThirdGroup<T> {
    data: T;
    thirdGroupIDs: string[];
    hasThirdGroupScopes: boolean;
}
interface ValidateFourthGroup<T> {
    data: T;
    fourthGroupIDs: string[];
    hasFourthGroupScopes: boolean;
}
interface ValidatePersonal<T> {
    data: T;
    userID: string | undefined;
    hasPersonalScopes: boolean;
}
export interface Credentials {
    userID?: string;
    phoneNumber?: string;
    email?: string;
    roles?: string[];
    hasPersonalScopes?: boolean;
    hasSecondGroupScopes?: boolean;
    hasFirstGroupScopes?: boolean;
    status?: string;
    secondGroupIDs?: string[];
    firstGroupIDs?: string[];
    isAdmin?: boolean;
    isPublic?: boolean;
}
declare type getKeyIDFunction = <T>(data: T) => string[];
export declare class AccessValidator {
    constructor(personalKey?: string[], firstGroupKey?: string[], secondGroupKey?: string[], thirdGroupKey?: string[], fourthGroupKey?: string[], firstGroupKeyNameForIDs?: string, secondGroupKeyNameForIDs?: string, thirdGroupKeyNameForIDs?: string, fourthGroupKeyNameForIDs?: string, firstGroupKeyNameForScopes?: string, secondGroupKeyNameForScopes?: string, thirdGroupKeyNameForScopes?: string, fourthGroupKeyNameForScopes?: string, getPersonalIDs?: getKeyIDFunction, getFirstGroupIDs?: getKeyIDFunction, getSecondGroupIDs?: getKeyIDFunction, getThirdGroupIDs?: getKeyIDFunction, getFourthGroupIDs?: getKeyIDFunction);
    personalKey: string[];
    firstGroupKey: string[];
    secondGroupKey: string[];
    thirdGroupKey: string[];
    fourthGroupKey: string[];
    firstGroupKeyNameForIDs: string;
    secondGroupKeyNameForIDs: string;
    thirdGroupKeyNameForIDs: string;
    fourthGroupKeyNameForIDs: string;
    firstGroupKeyNameForScopes: string;
    secondGroupKeyNameForScopes: string;
    thirdGroupKeyNameForScopes: string;
    fourthGroupKeyNameForScopes: string;
    getPersonalIDs: getKeyIDFunction;
    getFirstGroupIDs: getKeyIDFunction;
    getSecondGroupIDs: getKeyIDFunction;
    getThirdGroupIDs: getKeyIDFunction;
    getFourthGroupIDs: getKeyIDFunction;
    preProcess: ({ credentials }: {
        credentials: Credentials;
    }) => {
        shouldValidate3rdAnd4th: any;
        firstGroupIDs: any;
        secondGroupIDs: any;
        thirdGroupIDs: any;
        fourthGroupIDs: any;
        hasFirstGroupScopes: any;
        hasSecondGroupScopes: any;
        hasThirdGroupScopes: any;
        hasFourthGroupScopes: any;
        userID: string | undefined;
        isAdmin: boolean;
        isPublic: boolean;
        hasPersonalScopes: boolean;
    };
    validatePersonal: <T>({ data, userID, hasPersonalScopes }: ValidatePersonal<T>) => boolean | "" | undefined;
    validateFirstGroup: <T>({ data, firstGroupIDs, hasFirstGroupScopes }: ValidateFirstGroup<T>) => boolean;
    validateSecondGroup: <T>({ data, secondGroupIDs, hasSecondGroupScopes }: ValidateSecondGroup<T>) => boolean;
    validateThirdGroup: <T>({ data, thirdGroupIDs, hasThirdGroupScopes }: ValidateThirdGroup<T>) => boolean;
    validateFourthGroup: <T>({ data, fourthGroupIDs, hasFourthGroupScopes }: ValidateFourthGroup<T>) => boolean;
    validateAccessToSingle: <T>({ data, credentials }: ValidateAccessToSingle<T>) => {
        validData: null;
    } | {
        validData: T;
    };
    validateAccessToList: <T>({ data, credentials }: ValidateAccessToList<T>) => {
        validData: (T | null)[];
        validDataLength: number;
    };
}
export {};
