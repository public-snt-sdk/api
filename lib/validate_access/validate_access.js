"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessValidator = void 0;
const helper_1 = require("./helper");
class AccessValidator {
    constructor(personalKey = ['createdBy'], firstGroupKey = ['merchantID'], secondGroupKey = ['storeID'], thirdGroupKey = ['folder'], fourthGroupKey = ['file'], firstGroupKeyNameForIDs = 'merchantIDs', secondGroupKeyNameForIDs = 'storeIDs', thirdGroupKeyNameForIDs = 'folderIDs', fourthGroupKeyNameForIDs = 'fileIDs', firstGroupKeyNameForScopes = 'hasMerchantScopes', secondGroupKeyNameForScopes = 'hasStoreScopes', thirdGroupKeyNameForScopes = 'hasFolderScopes', fourthGroupKeyNameForScopes = 'hasFileScopes', getPersonalIDs, getFirstGroupIDs, getSecondGroupIDs, getThirdGroupIDs, getFourthGroupIDs) {
        this.getPersonalIDs = (data) => {
            const personalIDs = helper_1.getIDsInObject({
                data,
                keys: this.personalKey,
            });
            return personalIDs;
        };
        this.getFirstGroupIDs = (data) => {
            const firstGroupID = helper_1.getIDsInObject({
                data,
                keys: this.firstGroupKey,
            });
            return firstGroupID;
        };
        this.getSecondGroupIDs = (data) => {
            const secondGroupID = helper_1.getIDsInObject({
                data,
                keys: this.secondGroupKey,
            });
            return secondGroupID;
        };
        this.getThirdGroupIDs = (data) => {
            const thirdGroupID = helper_1.getIDsInObject({
                data,
                keys: this.thirdGroupKey,
            });
            return thirdGroupID;
        };
        this.getFourthGroupIDs = (data) => {
            const fourthGroupID = helper_1.getIDsInObject({
                data,
                keys: this.fourthGroupKey,
            });
            return fourthGroupID;
        };
        this.preProcess = ({ credentials }) => {
            const shouldValidate3rdAnd4th = credentials[this.thirdGroupKeyNameForScopes]
                && credentials[this.fourthGroupKeyNameForScopes];
            const firstGroupIDs = credentials[this.firstGroupKeyNameForIDs] || [];
            const secondGroupIDs = credentials[this.secondGroupKeyNameForIDs] || [];
            const thirdGroupIDs = credentials[this.thirdGroupKeyNameForIDs] || [];
            const fourthGroupIDs = credentials[this.fourthGroupKeyNameForIDs] || [];
            const hasFirstGroupScopes = credentials[this.firstGroupKeyNameForScopes] || false;
            const hasSecondGroupScopes = credentials[this.secondGroupKeyNameForScopes] || false;
            const hasThirdGroupScopes = credentials[this.thirdGroupKeyNameForScopes] || false;
            const hasFourthGroupScopes = credentials[this.fourthGroupKeyNameForScopes] || false;
            return {
                shouldValidate3rdAnd4th,
                firstGroupIDs,
                secondGroupIDs,
                thirdGroupIDs,
                fourthGroupIDs,
                hasFirstGroupScopes,
                hasSecondGroupScopes,
                hasThirdGroupScopes,
                hasFourthGroupScopes,
                userID: credentials.userID,
                isAdmin: credentials.isAdmin || false,
                isPublic: credentials.isPublic || false,
                hasPersonalScopes: credentials.hasPersonalScopes || false,
            };
        };
        this.validatePersonal = ({ data, userID, hasPersonalScopes }) => {
            const personalIDsFromdata = this.getPersonalIDs(data);
            const isOwned = userID && (personalIDsFromdata === null || personalIDsFromdata === void 0 ? void 0 : personalIDsFromdata.includes(userID)) && hasPersonalScopes;
            return isOwned;
        };
        this.validateFirstGroup = ({ data, firstGroupIDs, hasFirstGroupScopes }) => {
            const firstGroupIDsFromData = this.getFirstGroupIDs(data);
            const isInFirstGroup = (firstGroupIDsFromData === null || firstGroupIDsFromData === void 0 ? void 0 : firstGroupIDsFromData.some((eachID) => firstGroupIDs === null || firstGroupIDs === void 0 ? void 0 : firstGroupIDs.includes(eachID))) && hasFirstGroupScopes;
            return isInFirstGroup;
        };
        this.validateSecondGroup = ({ data, secondGroupIDs, hasSecondGroupScopes }) => {
            const secondGroupIDsFromData = this.getSecondGroupIDs(data);
            const isInSecondGroup = (secondGroupIDsFromData === null || secondGroupIDsFromData === void 0 ? void 0 : secondGroupIDsFromData.some((eachID) => secondGroupIDs === null || secondGroupIDs === void 0 ? void 0 : secondGroupIDs.includes(eachID))) && hasSecondGroupScopes;
            return isInSecondGroup;
        };
        this.validateThirdGroup = ({ data, thirdGroupIDs, hasThirdGroupScopes }) => {
            const thirdGroupIDsFromData = this.getThirdGroupIDs(data);
            const isInThirdGroup = (thirdGroupIDsFromData === null || thirdGroupIDsFromData === void 0 ? void 0 : thirdGroupIDsFromData.some((eachID) => thirdGroupIDs === null || thirdGroupIDs === void 0 ? void 0 : thirdGroupIDs.includes(eachID))) && hasThirdGroupScopes;
            return isInThirdGroup;
        };
        this.validateFourthGroup = ({ data, fourthGroupIDs, hasFourthGroupScopes }) => {
            const fourthGroupIDsFromData = this.getFourthGroupIDs(data);
            const isInFourthGroup = (fourthGroupIDsFromData === null || fourthGroupIDsFromData === void 0 ? void 0 : fourthGroupIDsFromData.some((eachID) => fourthGroupIDs === null || fourthGroupIDs === void 0 ? void 0 : fourthGroupIDs.includes(eachID))) && hasFourthGroupScopes;
            return isInFourthGroup;
        };
        this.validateAccessToSingle = ({ data, credentials }) => {
            if (!data || Array.isArray(data)) {
                return {
                    validData: null,
                };
            }
            const { shouldValidate3rdAnd4th, firstGroupIDs, secondGroupIDs, thirdGroupIDs, fourthGroupIDs, hasFirstGroupScopes, hasSecondGroupScopes, hasThirdGroupScopes, hasFourthGroupScopes, userID, isAdmin, isPublic, hasPersonalScopes, } = this.preProcess({ credentials });
            if (isAdmin || isPublic) {
                return {
                    validData: data,
                };
            }
            const isInFirstGroup = this.validateFirstGroup({ data, firstGroupIDs, hasFirstGroupScopes });
            if (isInFirstGroup && !shouldValidate3rdAnd4th) {
                return {
                    validData: data,
                };
            }
            const isInSecondGroup = this.validateSecondGroup({ data, secondGroupIDs, hasSecondGroupScopes });
            if (isInSecondGroup && !shouldValidate3rdAnd4th) {
                return {
                    validData: data,
                };
            }
            const isOwned = this.validatePersonal({ data, userID, hasPersonalScopes });
            if (isOwned && !shouldValidate3rdAnd4th) {
                return {
                    validData: data,
                };
            }
            if (!isOwned && !isInFirstGroup && !isInSecondGroup && !shouldValidate3rdAnd4th) {
                throw {
                    code: 403,
                    name: 'ValidationError',
                };
            }
            const isInThirdGroup = this.validateThirdGroup({ data, thirdGroupIDs, hasThirdGroupScopes });
            if (isInThirdGroup) {
                return {
                    validData: data,
                };
            }
            const isInFourthGroup = this.validateFourthGroup({ data, fourthGroupIDs, hasFourthGroupScopes });
            if (isInFourthGroup) {
                return {
                    validData: data,
                };
            }
            throw {
                code: 403,
                name: 'ValidationError',
            };
        };
        this.validateAccessToList = ({ data, credentials }) => {
            if (!(data === null || data === void 0 ? void 0 : data.length)) {
                return {
                    validData: [],
                    validDataLength: 0,
                };
            }
            const { shouldValidate3rdAnd4th, firstGroupIDs, secondGroupIDs, thirdGroupIDs, fourthGroupIDs, hasFirstGroupScopes, hasSecondGroupScopes, hasThirdGroupScopes, hasFourthGroupScopes, userID, isAdmin, isPublic, hasPersonalScopes, } = this.preProcess({ credentials });
            if (isAdmin || isPublic) {
                return {
                    validData: data,
                    validDataLength: data === null || data === void 0 ? void 0 : data.length,
                };
            }
            let validDataLength = 0;
            const validData = data.map(each => {
                const isInFirstGroup = this.validateFirstGroup({ data: each, firstGroupIDs, hasFirstGroupScopes });
                if (isInFirstGroup && !shouldValidate3rdAnd4th) {
                    ++validDataLength;
                    return each;
                }
                const isInSecondGroup = this.validateSecondGroup({ data: each, secondGroupIDs, hasSecondGroupScopes });
                if (isInSecondGroup && !shouldValidate3rdAnd4th) {
                    ++validDataLength;
                    return each;
                }
                const isOwned = this.validatePersonal({ data: each, userID, hasPersonalScopes });
                if (isOwned && !shouldValidate3rdAnd4th) {
                    ++validDataLength;
                    return each;
                }
                if (!isOwned && !isInFirstGroup && !isInSecondGroup && !shouldValidate3rdAnd4th) {
                    return null;
                }
                const isInThirdGroup = this.validateThirdGroup({ data: each, thirdGroupIDs, hasThirdGroupScopes });
                if (isInThirdGroup) {
                    ++validDataLength;
                    return each;
                }
                const isInFourthGroup = this.validateFourthGroup({ data: each, fourthGroupIDs, hasFourthGroupScopes });
                if (isInFourthGroup) {
                    ++validDataLength;
                    return each;
                }
                return null;
            });
            return {
                validData,
                validDataLength,
            };
        };
        this.personalKey = personalKey;
        this.firstGroupKey = firstGroupKey;
        this.secondGroupKey = secondGroupKey;
        this.thirdGroupKey = thirdGroupKey;
        this.fourthGroupKey = fourthGroupKey;
        this.firstGroupKeyNameForIDs = firstGroupKeyNameForIDs;
        this.secondGroupKeyNameForIDs = secondGroupKeyNameForIDs;
        this.thirdGroupKeyNameForIDs = thirdGroupKeyNameForIDs;
        this.fourthGroupKeyNameForIDs = fourthGroupKeyNameForIDs;
        this.firstGroupKeyNameForScopes = firstGroupKeyNameForScopes;
        this.secondGroupKeyNameForScopes = secondGroupKeyNameForScopes;
        this.thirdGroupKeyNameForScopes = thirdGroupKeyNameForScopes;
        this.fourthGroupKeyNameForScopes = fourthGroupKeyNameForScopes;
        if (getPersonalIDs)
            this.getFirstGroupIDs = getPersonalIDs;
        if (getFirstGroupIDs)
            this.getFirstGroupIDs = getFirstGroupIDs;
        if (getSecondGroupIDs)
            this.getSecondGroupIDs = getSecondGroupIDs;
        if (getThirdGroupIDs)
            this.getThirdGroupIDs = getThirdGroupIDs;
        if (getFourthGroupIDs)
            this.getFourthGroupIDs = getFourthGroupIDs;
    }
}
exports.AccessValidator = AccessValidator;
//# sourceMappingURL=validate_access.js.map