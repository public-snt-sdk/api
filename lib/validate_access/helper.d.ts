interface GetIDInObject {
    data: any;
    keys: string[];
}
export declare const getIDsInObject: ({ data, keys }: GetIDInObject) => string[];
export {};
