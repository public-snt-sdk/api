"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getIDsInObject = void 0;
exports.getIDsInObject = ({ data, keys }) => {
    const ids = [];
    for (const key of keys) {
        if (!data[key]) {
            continue;
        }
        ids.push(data[key]);
    }
    return ids;
};
//# sourceMappingURL=helper.js.map