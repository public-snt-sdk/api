interface IsTruThy {
    and?: any[];
    or?: any[];
    errorName: string;
}
interface IsNotEmpty {
    entity: any[];
    errorName: string;
    returnData?: any;
}
export declare class Assertion {
    constructor(errors: object, httpStatusCode: number);
    errors: object;
    httpStatusCode: number;
    isNotEmpty: ({ entity, errorName, returnData }: IsNotEmpty) => any;
    isEmpty: ({ entity, errorName, returnData }: IsNotEmpty) => any;
    isTruthy: ({ and, or, errorName }: IsTruThy) => boolean;
    isFalsy: ({ and, or, errorName }: IsTruThy) => boolean;
}
export {};
