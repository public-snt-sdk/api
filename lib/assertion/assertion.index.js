"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Assertion = void 0;
class Assertion {
    constructor(errors, httpStatusCode) {
        this.isNotEmpty = ({ entity, errorName = 'NOT_EMPTY', returnData }) => {
            const message = this.errors[errorName];
            if (!message) {
                throw new Error('need to update error message into errors parameter');
            }
            if (!returnData && entity && entity.length > 0) {
                return true;
            }
            if (returnData && entity && entity.length > 0) {
                return returnData;
            }
            throw {
                code: this.httpStatusCode,
                message,
            };
        };
        this.isEmpty = ({ entity, errorName = 'EMPTY', returnData }) => {
            const message = this.errors[errorName];
            if (!message) {
                throw new Error('need to update error message into errors parameter');
            }
            if (!returnData && entity && entity.length === 0) {
                return true;
            }
            if (returnData && entity && entity.length === 0) {
                return returnData;
            }
            throw {
                code: this.httpStatusCode,
                message,
            };
        };
        this.isTruthy = ({ and, or, errorName = 'NOT_TRUTHY' }) => {
            const message = this.errors[errorName];
            if (!message) {
                throw new Error('need to update error message into errors parameter');
            }
            if (and && or) {
                throw new Error('need to use either and/or parameter, not both of them');
            }
            const isAnd = and && and.every((each) => !!each);
            if (isAnd) {
                return true;
            }
            const isOr = or && or.some((each) => !!each);
            if (isOr) {
                return true;
            }
            throw {
                code: this.httpStatusCode,
                message,
            };
        };
        this.isFalsy = ({ and, or, errorName = 'NOT_FALSY' }) => {
            const message = this.errors[errorName];
            if (!message) {
                throw new Error('need to update error message into errors parameter');
            }
            if (and && or) {
                throw new Error('need to use either and/or parameter, not both of them');
            }
            const isAnd = !!(and === null || and === void 0 ? void 0 : and.length) && and.every((each) => !!each);
            if (!isAnd && !or) {
                return true;
            }
            const isOr = !!(or === null || or === void 0 ? void 0 : or.length) && or.some((each) => !!each);
            if (!isOr && !and) {
                return true;
            }
            throw {
                code: this.httpStatusCode,
                message,
            };
        };
        this.errors = errors;
        this.httpStatusCode = httpStatusCode;
    }
}
exports.Assertion = Assertion;
//# sourceMappingURL=assertion.index.js.map