declare type step = (input?: any) => any;
interface Chain {
    chain: step[];
}
export declare const process: ({ chain }: Chain) => Promise<any>;
export {};
