interface AccessTokens {
    accessToken: string;
}
interface User {
    [key: string]: any;
}
interface GenerateAccessTokens {
    users: User[];
    jwtSecret: string;
}
export declare const generateTokens: ({ users, jwtSecret }: GenerateAccessTokens) => AccessTokens[];
export {};
