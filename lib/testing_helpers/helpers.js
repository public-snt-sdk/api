"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateTokens = void 0;
const jsonwebtoken = require("jsonwebtoken");
exports.generateTokens = ({ users, jwtSecret }) => {
    const accessTokens = [];
    for (const user of users) {
        const payload = {
            userID: user.userID || user._id.toString(),
            status: user.status,
        };
        const accessToken = jsonwebtoken.sign(payload, jwtSecret, { expiresIn: '30d' });
        accessTokens.push({ accessToken });
    }
    return accessTokens;
};
//# sourceMappingURL=helpers.js.map