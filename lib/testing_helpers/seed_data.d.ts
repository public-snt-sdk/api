export interface ItemData {
    schemaName: string;
    data: RecordData[];
}
interface IndependentItems {
    [key: string]: ItemData[];
}
interface TypeormConnection {
    readonly name: string;
    connect(): Promise<this>;
    close(): Promise<void>;
    [key: string]: any;
}
interface MongooseConnection {
    close(force?: boolean): Promise<void>;
    name: string;
    [key: string]: any;
}
interface HandleDependentData {
    dependentItemsData: ItemData[];
    connection: TypeormConnection | MongooseConnection;
}
interface RecordData {
    [key: string]: any;
}
export declare class SeedDataGenerator {
    usersData: ItemData;
    firstGroupData: ItemData;
    secondGroupData: ItemData;
    relationshipData: ItemData;
    constructor(usersData: ItemData, firstGroupData: ItemData, secondGroupData: ItemData, RelationshipData: ItemData);
    connectDatabase(): Promise<TypeormConnection | MongooseConnection>;
    saveDataToDatabase(_item: ItemData, _connection: TypeormConnection | MongooseConnection): Promise<void>;
    handleDependentData({ dependentItemsData, connection }: HandleDependentData): Promise<void>;
    run(independentItems?: IndependentItems): Promise<void>;
}
export {};
