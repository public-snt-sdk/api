"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedDataGenerator = void 0;
const concurency_1 = require("../migration/concurency");
class SeedDataGenerator {
    constructor(usersData, firstGroupData, secondGroupData, RelationshipData) {
        this.usersData = usersData;
        this.firstGroupData = firstGroupData;
        this.secondGroupData = secondGroupData;
        this.relationshipData = RelationshipData;
        return this;
    }
    connectDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error('you have to override the connectDatabase() function to use it');
        });
    }
    saveDataToDatabase(_item, _connection) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error('you have to override the saveDataToDatabase() function with two parameters are item and connection in order to use it');
        });
    }
    handleDependentData({ dependentItemsData, connection }) {
        return __awaiter(this, void 0, void 0, function* () {
            if (dependentItemsData.length === 1) {
                yield this.saveDataToDatabase(dependentItemsData[0], connection);
                return;
            }
            const promises = [];
            for (const itemData of dependentItemsData) {
                const task = () => __awaiter(this, void 0, void 0, function* () {
                    yield this.saveDataToDatabase(itemData, connection);
                });
                promises.push(task);
            }
            yield concurency_1.runConcurrency(1, promises);
        });
    }
    run(independentItems = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const connection = yield this.connectDatabase();
            this.relationshipData.data.forEach(record => delete record.accessToken);
            yield this.handleDependentData({
                connection,
                dependentItemsData: [
                    this.usersData,
                    this.firstGroupData,
                    this.secondGroupData,
                    this.relationshipData,
                ],
            });
            if (independentItems) {
                const promises = [];
                for (const key in independentItems) {
                    if (!independentItems[key] || !independentItems[key].length) {
                        continue;
                    }
                    const itemsData = independentItems[key];
                    const task = () => __awaiter(this, void 0, void 0, function* () {
                        yield this.handleDependentData({
                            connection,
                            dependentItemsData: itemsData,
                        });
                    });
                    promises.push(task);
                }
                yield concurency_1.runConcurrency(5, promises);
            }
            yield connection.close();
        });
    }
}
exports.SeedDataGenerator = SeedDataGenerator;
//# sourceMappingURL=seed_data.js.map