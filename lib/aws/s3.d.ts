export interface PutSignedUrlService {
    fileName: string;
    fileExtension: string;
    expires?: number;
}
declare type getDefaultUrlFunction = () => string;
declare type getExpiresFunction = () => number;
interface ClientConfiguration {
    accessKeyId: string;
    secretAccessKey: string;
    region: string;
    signatureVersion: string;
}
export declare class S3Aws {
    private s3Bucket;
    constructor(s3Config: ClientConfiguration, bucket: string, ACL?: string, expires?: number, getDefaultUrl?: getDefaultUrlFunction);
    s3Config: ClientConfiguration;
    bucket: string;
    ACL?: string;
    expires?: number;
    getDefaultUrl: getDefaultUrlFunction;
    getACL: getDefaultUrlFunction;
    getExpires: getExpiresFunction;
    putSignedUrl({ fileName, fileExtension, expires }: PutSignedUrlService): Promise<{
        signedUrl: string;
        returnUrl: string;
    }>;
}
export {};
