"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var s3_1 = require("./s3");
Object.defineProperty(exports, "S3Aws", { enumerable: true, get: function () { return s3_1.S3Aws; } });
var ses_1 = require("./ses");
Object.defineProperty(exports, "SesService", { enumerable: true, get: function () { return ses_1.SesService; } });
//# sourceMappingURL=aws.index.js.map