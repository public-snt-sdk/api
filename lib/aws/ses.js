"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SesService = void 0;
const aws_sdk_1 = require("aws-sdk");
class SesService {
    constructor(sesConfig, senderDefault) {
        this.sesBucket = new aws_sdk_1.SES(sesConfig);
        if (senderDefault) {
            this.senderDefault = senderDefault;
        }
    }
    sendEmail({ params }) {
        return new Promise((resolve, reject) => {
            const sender = `Sender Name <${this.senderDefault}>`;
            const Source = (params === null || params === void 0 ? void 0 : params.Source) ? params === null || params === void 0 ? void 0 : params.Source : sender;
            this.sesBucket.sendEmail(params = Object.assign(Object.assign({}, params), { Source }), (err, data) => {
                if (err) {
                    reject({
                        code: err.statusCode,
                        message: err.message,
                    });
                }
                resolve(data);
            });
        });
    }
}
exports.SesService = SesService;
//# sourceMappingURL=ses.js.map