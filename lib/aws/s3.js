"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3Aws = void 0;
const aws_sdk_1 = require("aws-sdk");
class S3Aws {
    constructor(s3Config, bucket, ACL, expires, getDefaultUrl) {
        this.getDefaultUrl = () => {
            var _a;
            const defaultUrl = `http://s3.${(_a = this.s3Config) === null || _a === void 0 ? void 0 : _a.region}.amazonaws.com/${this.bucket}/`;
            return defaultUrl;
        };
        this.getACL = () => {
            const acl = this.ACL ? this.ACL : 'public-read';
            return acl;
        };
        this.getExpires = () => {
            const expires = this.expires ? this.expires : 1800;
            return expires;
        };
        this.s3Bucket = new aws_sdk_1.S3(s3Config);
        this.bucket = bucket;
        this.s3Config = s3Config;
        if (ACL)
            this.ACL = ACL;
        if (expires)
            this.expires = expires;
        if (getDefaultUrl)
            this.getDefaultUrl = getDefaultUrl;
    }
    putSignedUrl({ fileName, fileExtension, expires }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const getDefaultUrl = this.getDefaultUrl();
                const getACL = this.getACL();
                const getExpires = expires ? expires : this.getExpires();
                const s3Link = `${Date.now()}-${fileName}.${fileExtension}`;
                const signedUrl = yield this.s3Bucket.getSignedUrlPromise('putObject', {
                    ACL: getACL,
                    Bucket: this.bucket,
                    Key: `${s3Link}`,
                    Expires: getExpires,
                });
                return {
                    signedUrl,
                    returnUrl: `${getDefaultUrl}${s3Link}`,
                };
            }
            catch (error) {
                return Promise.reject(error);
            }
        });
    }
}
exports.S3Aws = S3Aws;
//# sourceMappingURL=s3.js.map