declare type Address = string;
declare type AddressList = Address[];
interface DestinationData {
    ToAddresses?: AddressList;
    CcAddresses?: AddressList;
    BccAddresses?: AddressList;
}
interface Content {
    Data: string;
    Charset?: string;
}
interface Body {
    Text?: Content;
    Html?: Content;
}
interface MessageData {
    Subject: Content;
    Body: Body;
}
interface MessageTagList {
    Name: string;
    Value: string;
}
export interface ParamsSesService {
    Source?: Address;
    Destination: DestinationData;
    Message: MessageData;
    ReplyToAddresses?: AddressList;
    ReturnPath?: Address;
    SourceArn?: string;
    ReturnPathArn?: string;
    Tags?: MessageTagList[];
    ConfigurationSetName?: string;
}
export interface SendEmailRequest {
    params: ParamsSesService;
}
export {};
