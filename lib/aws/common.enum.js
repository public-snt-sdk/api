"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtensionsSupport = exports.extensionsSupport = void 0;
exports.extensionsSupport = ['img', 'pdf', 'jpg', 'jpeg', 'png', 'svg', 'xls', 'xlsx', 'ai', 'eps', 'epsf', 'epsi',
    'mov', 'mp4', 'm4a', 'm4v', 'mpg', 'mpeg', 'wmv', 'avi', 'MPEG-4', 'DivX', 'MPEG-2', 'flv'];
var ExtensionsSupport;
(function (ExtensionsSupport) {
    ExtensionsSupport["img"] = "img";
    ExtensionsSupport["pdf"] = "pdf";
    ExtensionsSupport["jpg"] = "jpg";
    ExtensionsSupport["jpeg"] = "jpeg";
    ExtensionsSupport["png"] = "png";
    ExtensionsSupport["svg"] = "svg";
    ExtensionsSupport["xls"] = "xls";
    ExtensionsSupport["xlsx"] = "xlsx";
    ExtensionsSupport["ai"] = "ai";
    ExtensionsSupport["eps"] = "eps";
    ExtensionsSupport["epsf"] = "epsf";
    ExtensionsSupport["epsi"] = "epsi";
    ExtensionsSupport["mov"] = "mov";
    ExtensionsSupport["mp4"] = "mp4";
    ExtensionsSupport["m4a"] = "m4a";
    ExtensionsSupport["m4v"] = "m4v";
    ExtensionsSupport["mpg"] = "mpg";
    ExtensionsSupport["mpeg"] = "mpeg";
    ExtensionsSupport["wmv"] = "wmv";
    ExtensionsSupport["avi"] = "avi";
    ExtensionsSupport["MPEG_4"] = "MPEG-4";
    ExtensionsSupport["DivX"] = "DivX";
    ExtensionsSupport["MPE_2"] = "MPEG-2";
    ExtensionsSupport["flv"] = "flv";
})(ExtensionsSupport = exports.ExtensionsSupport || (exports.ExtensionsSupport = {}));
//# sourceMappingURL=common.enum.js.map