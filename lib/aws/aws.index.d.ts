export { S3Aws } from './s3';
export { ParamsSesService, SendEmailRequest } from './ses.interface';
export { SesService } from './ses';
