import { SendEmailRequest } from './ses.interface';
interface SesConfig {
    accessKeyId: string;
    secretAccessKey: string;
    region: string;
}
export declare class SesService {
    private sesBucket;
    constructor(sesConfig: SesConfig, senderDefault?: string);
    senderDefault?: string;
    sendEmail({ params }: SendEmailRequest): Promise<unknown>;
}
export {};
